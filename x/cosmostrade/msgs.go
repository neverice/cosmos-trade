package cosmostrade

import (
	"encoding/json"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// MsgCreateOrder defines the CreateOrder message
type MsgCreateOrder struct {
	Buy   sdk.Coins
	Sell  sdk.Coins
	Owner sdk.AccAddress
}

// NewMsgCreateOrder is the constructor function for MsgCreateOrder
func NewMsgCreateOrder(buy sdk.Coins, sell sdk.Coins, owner sdk.AccAddress) MsgCreateOrder {
	return MsgCreateOrder{
		Buy:   buy,
		Sell:  sell,
		Owner: owner,
	}
}

// Route should return the name of the module
func (msg MsgCreateOrder) Route() string { return "cosmostrade" }

// Type should return the action
func (msg MsgCreateOrder) Type() string { return "create_order" }

// ValidateBasic runs stateless checks on the message
func (msg MsgCreateOrder) ValidateBasic() sdk.Error {
	if msg.Owner.Empty() {
		return sdk.ErrInvalidAddress(msg.Owner.String())
	}
	if len(msg.Buy) == 0 || len(msg.Sell) == 0 {
		return sdk.ErrUnknownRequest("Buy and/or Sell cannot be empty")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgCreateOrder) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

// GetSigners defines whose signature is required
func (msg MsgCreateOrder) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Owner}
}

/////////////////////////---------MsgCompleteOrder--------/////////////////////////

// MsgCompleteOrder defines the CompleteOrder message
type MsgCompleteOrder struct {
	OrderID string
	Buyer   sdk.AccAddress
}

// NewMsgCompleteOrder is the constructor function for MsgCompleteOrder
func NewMsgCompleteOrder(orderID string, buyer sdk.AccAddress) MsgCompleteOrder {
	return MsgCompleteOrder{
		OrderID: orderID,
		Buyer:   buyer,
	}
}

// Route should return the name of the module
func (msg MsgCompleteOrder) Route() string { return "cosmostrade" }

// Type should return the action
func (msg MsgCompleteOrder) Type() string { return "complete_order" }

// ValidateBasic runs stateless checks on the message
func (msg MsgCompleteOrder) ValidateBasic() sdk.Error {
	if msg.Buyer.Empty() {
		return sdk.ErrInvalidAddress(msg.Buyer.String())
	}
	if len(msg.OrderID) == 0 {
		return sdk.ErrUnknownRequest("Name cannot be empty")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgCompleteOrder) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

// GetSigners defines whose signature is required
func (msg MsgCompleteOrder) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Buyer}
}

/////////////////////////---------MsgCancelOrder--------/////////////////////////

// MsgCancelOrder defines the CancelOrder message
type MsgCancelOrder struct {
	OrderID string
	Owner   sdk.AccAddress
}

// NewMsgCancelOrder is the constructor function for CancelOrder
func NewMsgCancelOrder(orderID string, owner sdk.AccAddress) MsgCancelOrder {
	return MsgCancelOrder{
		OrderID: orderID,
		Owner:   owner,
	}
}

// Route should return the name of the module
func (msg MsgCancelOrder) Route() string { return "cosmostrade" }

// Type should return the action
func (msg MsgCancelOrder) Type() string { return "cancel_order" }

// ValidateBasic runs stateless checks on the message
func (msg MsgCancelOrder) ValidateBasic() sdk.Error {
	if msg.Owner.Empty() {
		return sdk.ErrInvalidAddress(msg.Owner.String())
	}
	if len(msg.OrderID) == 0 {
		return sdk.ErrUnknownRequest("OrderID cannot be empty")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgCancelOrder) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

// GetSigners defines whose signature is required
func (msg MsgCancelOrder) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Owner}
}
