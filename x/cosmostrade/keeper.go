package cosmostrade

import (
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/cosmos/cosmos-sdk/x/bank"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// Keeper maintains the link to data storage and exposes getter/setter methods for the various parts of the state machine
type Keeper struct {
	coinKeeper bank.Keeper

	storeKey sdk.StoreKey // Unexposed key to access store from sdk.Context

	cdc *codec.Codec // The wire codec for binary encoding/decoding.
}

// NewKeeper creates new instances of the cosmostrade Keeper
func NewKeeper(coinKeeper bank.Keeper, storeKey sdk.StoreKey, cdc *codec.Codec) Keeper {
	return Keeper{
		coinKeeper: coinKeeper,
		storeKey:   storeKey,
		cdc:        cdc,
	}
}

func (k Keeper) GetOrder(ctx sdk.Context, orderId string) *Order {
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(orderId)) {
		return nil
	}
	bz := store.Get([]byte(orderId))
	var order Order
	k.cdc.MustUnmarshalBinaryBare(bz, &order)
	return &order
}

func (k Keeper) CreateOrder(ctx sdk.Context, owner sdk.AccAddress, sell sdk.Coins, buy sdk.Coins) string {
	var order = NewOrder()
	order.Owner = owner
	order.Sell = sell
	order.Buy = buy
	store := ctx.KVStore(k.storeKey)
	store.Set([]byte(order.OrderID), k.cdc.MustMarshalBinaryBare(order))
	return order.OrderID
}

func (k Keeper) CancelOrder(ctx sdk.Context, order Order) {
	store := ctx.KVStore(k.storeKey)
	if store.Has([]byte(order.OrderID)) {
		store.Delete([]byte(order.OrderID))
	}
}

func (k Keeper) CompleteOrder(ctx sdk.Context, order Order, buyer sdk.AccAddress) {
	store := ctx.KVStore(k.storeKey)
	if store.Has([]byte(order.OrderID)) {
		order.Buyer = buyer
		order.Completed = time.Now()
		store.Set([]byte(order.OrderID), k.cdc.MustMarshalBinaryBare(order))
	}
}

func (k Keeper) GetOrders(ctx sdk.Context) []Order {
	var orders []Order
	ordersIterator := k.GetOrdersIterator(ctx)

	for ; ordersIterator.Valid(); ordersIterator.Next() {
		var order Order
		k.cdc.MustUnmarshalBinaryBare(ordersIterator.Value(), &order)
		if order.Buyer.Empty() {
			orders = append(orders, order)
		}
	}

	return orders
}

func (k Keeper) GetAccOrders(ctx sdk.Context, owner sdk.AccAddress) []Order {
	var orders []Order
	ordersIterator := k.GetOrdersIterator(ctx)

	for ; ordersIterator.Valid(); ordersIterator.Next() {
		var order Order
		k.cdc.MustUnmarshalBinaryBare(ordersIterator.Value(), &order)
		if order.Owner.Equals(owner) {
			orders = append(orders, order)
		}
	}

	return orders
}

// Get an iterator over all orders
func (k Keeper) GetOrdersIterator(ctx sdk.Context) sdk.Iterator {
	store := ctx.KVStore(k.storeKey)
	return sdk.KVStorePrefixIterator(store, nil)
}
