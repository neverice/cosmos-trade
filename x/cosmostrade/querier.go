package cosmostrade

import (
	"strings"

	"github.com/cosmos/cosmos-sdk/codec"

	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"
)

// query endpoints supported by the cosmostrade Querier
const (
	QueryOrders   = "orders"
	QueryMyOrders = "my-orders"
)

// NewQuerier is the module level router for state queries
func NewQuerier(keeper Keeper) sdk.Querier {
	return func(ctx sdk.Context, path []string, req abci.RequestQuery) (res []byte, err sdk.Error) {
		switch path[0] {
		case QueryOrders:
			return queryOrders(ctx, req, keeper)
		case QueryMyOrders:
			return queryMyOrders(ctx, path[1:], req, keeper)
		default:
			return nil, sdk.ErrUnknownRequest("unknown cosmostrade query endpoint")
		}
	}
}

// nolint: unparam
func queryOrders(ctx sdk.Context, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	var orders = keeper.GetOrders(ctx)

	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, orders)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}

	return bz, nil
}

// nolint: unparam
func queryMyOrders(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	acc, err2 := sdk.AccAddressFromBech32(path[0])
	if err2 != nil {
		panic("could not parse account address")
	}
	var orders = keeper.GetAccOrders(ctx, acc)

	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, orders)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}

	return bz, nil
}

// Query Result Payload for a orders query
type QueryResOrders []string

// implement fmt.Stringer
func (n QueryResOrders) String() string {
	return strings.Join(n[:], "\n")
}
