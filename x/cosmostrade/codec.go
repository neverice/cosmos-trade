package cosmostrade

import (
	"github.com/cosmos/cosmos-sdk/codec"
)

// RegisterCodec registers concrete types on wire codec
func RegisterCodec(cdc *codec.Codec) {
	cdc.RegisterConcrete(MsgCreateOrder{}, "cosmostrade/CreateOrder", nil)
	cdc.RegisterConcrete(MsgCompleteOrder{}, "cosmostrade/CompleteOrder", nil)
	cdc.RegisterConcrete(MsgCancelOrder{}, "cosmostrade/CancelOrder", nil)
}
