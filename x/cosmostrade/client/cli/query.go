package cli

import (
	"fmt"

	cosmostrade "cosmos-trade/x/cosmostrade"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/spf13/cobra"
)

// GetCmdNames queries a list of all orders
func GetCmdOrders(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "orders",
		Short: "orders",
		//Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)

			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/orders", queryRoute), nil)
			if err != nil {
				fmt.Printf("could not get query orders\n")
				return nil
			}

			var out cosmostrade.OrdersList
			cdc.MustUnmarshalJSON(res, &out)
			return cliCtx.PrintOutput(out)
		},
	}
}

// GetCmdNames queries a list of all orders
func GetCmdMyOrders(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "my-orders [acc]",
		Short: "my-orders [acc]",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)

			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/my-orders/%s", queryRoute, args[0]), nil)
			if err != nil {
				fmt.Printf("could not get query orders\n")
				return nil
			}

			var out cosmostrade.OrdersList
			cdc.MustUnmarshalJSON(res, &out)
			return cliCtx.PrintOutput(out)
		},
	}
}
