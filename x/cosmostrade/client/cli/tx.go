package cli

import (
	"github.com/spf13/cobra"

	cosmostrade "cosmos-trade/x/cosmostrade"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/client/utils"
	"github.com/cosmos/cosmos-sdk/codec"

	sdk "github.com/cosmos/cosmos-sdk/types"
	authtxb "github.com/cosmos/cosmos-sdk/x/auth/client/txbuilder"
)

// GetCmdCreateOrder is the CLI command for sending a CreateOrder transaction
func GetCmdCreateOrder(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "create-order [buy] [sell]",
		Short: "create new order",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)

			txBldr := authtxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			if err := cliCtx.EnsureAccountExists(); err != nil {
				return err
			}

			buyCoins, err := sdk.ParseCoins(args[0])
			if err != nil {
				return err
			}

			sellCoins, err := sdk.ParseCoins(args[1])
			if err != nil {
				return err
			}

			msg := cosmostrade.NewMsgCreateOrder(buyCoins, sellCoins, cliCtx.GetFromAddress())
			err = msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true

			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg}, false)
		},
	}
}

// GetCmdCompleteOrder is the CLI command for sending a CompleteOrder transaction
func GetCmdCompleteOrder(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "complete-order [orderID]",
		Short: "complete published order, exchanging coins",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)

			txBldr := authtxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			if err := cliCtx.EnsureAccountExists(); err != nil {
				return err
			}

			msg := cosmostrade.NewMsgCompleteOrder(args[0], cliCtx.GetFromAddress())
			err := msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true

			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg}, false)
		},
	}
}

// GetCmdCancelOrder is the CLI command for sending a CancelOrder transaction
func GetCmdCancelOrder(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "cancel-order [orderID]",
		Short: "cancel prewiously created order that you own",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)

			txBldr := authtxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			if err := cliCtx.EnsureAccountExists(); err != nil {
				return err
			}

			msg := cosmostrade.NewMsgCancelOrder(args[0], cliCtx.GetFromAddress())
			err := msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true

			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg}, false)
		},
	}
}
