package client

import (
	cosmostradecmd "cosmos-trade/x/cosmostrade/client/cli"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/spf13/cobra"
	amino "github.com/tendermint/go-amino"
)

// ModuleClient exports all client functionality from this module
type ModuleClient struct {
	storeKey string
	cdc      *amino.Codec
}

func NewModuleClient(storeKey string, cdc *amino.Codec) ModuleClient {
	return ModuleClient{storeKey, cdc}
}

// GetQueryCmd returns the cli query commands for this module
func (mc ModuleClient) GetQueryCmd() *cobra.Command {
	// Group cosmostrade queries under a subcommand
	namesvcQueryCmd := &cobra.Command{
		Use:   "cosmostrade",
		Short: "Querying commands for the cosmostrade module",
	}

	namesvcQueryCmd.AddCommand(client.GetCommands(
		cosmostradecmd.GetCmdOrders(mc.storeKey, mc.cdc),
		cosmostradecmd.GetCmdMyOrders(mc.storeKey, mc.cdc),
	)...)

	return namesvcQueryCmd
}

// GetTxCmd returns the transaction commands for this module
func (mc ModuleClient) GetTxCmd() *cobra.Command {
	namesvcTxCmd := &cobra.Command{
		Use:   "cosmostrade",
		Short: "cosmostrade transactions subcommands",
	}

	namesvcTxCmd.AddCommand(client.PostCommands(
		cosmostradecmd.GetCmdCreateOrder(mc.cdc),
		cosmostradecmd.GetCmdCompleteOrder(mc.cdc),
		cosmostradecmd.GetCmdCancelOrder(mc.cdc),
	)...)

	return namesvcTxCmd
}
