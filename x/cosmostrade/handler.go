package cosmostrade

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// NewHandler returns a handler for "cosmostrade" type messages.
func NewHandler(keeper Keeper) sdk.Handler {
	return func(ctx sdk.Context, msg sdk.Msg) sdk.Result {
		switch msg := msg.(type) {
		case MsgCreateOrder:
			return handleMsgCreateOrder(ctx, keeper, msg)
		case MsgCompleteOrder:
			return handleMsgCompleteOrder(ctx, keeper, msg)
		case MsgCancelOrder:
			return handleMsgCancelOrder(ctx, keeper, msg)
		default:
			errMsg := fmt.Sprintf("Unrecognized cosmostrade Msg type: %v", msg.Type())
			return sdk.ErrUnknownRequest(errMsg).Result()
		}
	}
}

// Handle a message to set name
func handleMsgCreateOrder(ctx sdk.Context, keeper Keeper, msg MsgCreateOrder) sdk.Result {
	keeper.CreateOrder(ctx, msg.Owner, msg.Sell, msg.Buy)
	return sdk.Result{}
}

func handleMsgCompleteOrder(ctx sdk.Context, keeper Keeper, msg MsgCompleteOrder) sdk.Result {
	var order = keeper.GetOrder(ctx, msg.OrderID)
	if order == nil {
		return sdk.ErrUnknownRequest("Order can't be found").Result()
	}

	if order.Buyer != nil {
		return sdk.ErrInternal("Order alredy has been completed").Result()
	}

	_, err := keeper.coinKeeper.SendCoins(ctx, msg.Buyer, order.Owner, order.Buy)
	if err != nil {
		return sdk.ErrInsufficientCoins("Buyer does not have enough coins").Result()
	}
	_, err2 := keeper.coinKeeper.SendCoins(ctx, order.Owner, msg.Buyer, order.Sell)
	if err2 != nil {
		keeper.coinKeeper.SendCoins(ctx, order.Owner, msg.Buyer, order.Buy) //Cancel transaction
		return sdk.ErrInsufficientCoins("Seller does not have enough coins").Result()
	}

	keeper.CompleteOrder(ctx, *order, msg.Buyer)
	return sdk.Result{}
}

func handleMsgCancelOrder(ctx sdk.Context, keeper Keeper, msg MsgCancelOrder) sdk.Result {
	var order = keeper.GetOrder(ctx, msg.OrderID)

	if !msg.Owner.Equals(order.Owner) { // Checks if the the msg sender is the same as the current owner
		return sdk.ErrUnauthorized("Incorrect Owner").Result() // If not, throw an error
	}
	keeper.CancelOrder(ctx, *order)
	return sdk.Result{}
}
