package cosmostrade

import (
	"fmt"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	uuid "github.com/satori/go.uuid"
)

type Order struct {
	OrderID   string         `json:"OrderID"`
	Buy       sdk.Coins      `json:"Buy"`
	Sell      sdk.Coins      `json:"Sell"`
	Owner     sdk.AccAddress `json:"Owner"`
	Buyer     sdk.AccAddress `json:"Buyer"`
	Created   time.Time      `json:"Created"`
	Completed time.Time      `json:"Completed"`
}

func (n Order) String() string {
	return fmt.Sprintf(`OrderID: %s
	Owner: %s
	Buy: %s
	Sell: %s
	Buyer: %s`, n.OrderID, n.Owner, n.Buy, n.Sell, n.Buyer)
}

type OrdersList []Order

// implement fmt.Stringer
func (n OrdersList) String() string {
	var res string
	for _, element := range n {
		res = res + element.String() + "\n"
	}
	return res
}

func NewOrder() Order {
	return Order{
		OrderID: uuid.NewV4().String(),
		Created: time.Now(),
	}
}
