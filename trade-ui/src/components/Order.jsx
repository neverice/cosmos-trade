import React from 'react'
import { Component } from 'react'

class Order extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user,
      password: '12345678'
    }
  }

  render() {
    const { order } = this.props;
    let competeButton;
    if (this.state.user != '' && this.state.user != order.Owner) {
      competeButton = <button onClick={this.handleComplete} className="btn btn-primary float-right">Complete</button>
    }

    let cancelButton;
    if (this.state.user != '' && order.Owner == this.state.user && order.Buyer == "") {
      cancelButton = <button onClick={this.handleCancel} className="btn btn-primary float-right btn-space mr-2">Cancel</button>
    }

    let orderHeaderStyle;
    if (order.Buyer != "") {
      orderHeaderStyle = { background: 'green' };
    }

    //user={this.props.user}
    return (
      <div className="card" style={{ marginTop: '10px' }}>
        <div className="card-header" style={orderHeaderStyle}>
          <h2>Order: {order.OrderID}</h2>
        </div>
        <div className="card-body">
          <p>Owner: {order.Owner}</p>
          <p>Buyer: {order.Buyer}</p>
          <p>Buy: {order.Buy[0].amount} {order.Buy[0].denom}</p>
          <p>Sell: {order.Sell[0].amount} {order.Sell[0].denom}</p>
          <p>Created: {order.Created}</p>
          {competeButton}
          {cancelButton}
        </div>
      </div>
    )
  }

  handleComplete = () => {
    fetch("http://localhost:1317/auth/accounts/" + this.state.user)
      .then(results => {
        return results.json();
      }).then(account => {
        let requestBody = JSON.stringify({
          base_req: {
            from: this.state.user,
            password: this.state.password,
            chain_id: 'tradechain',
            sequence: account.sequence,
            account_number: account.account_number
          },
          orderID: this.props.order.OrderID,
          buyer: this.state.user
        });

        console.log(requestBody)

        fetch('http://localhost:1317/cosmostrade/orders/complete', {
          method: 'POST',
          mode: 'no-cors',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: requestBody
        }).then(response => {
          console.log(response);
        });
      });
  }

  handleCancel = () => {

    fetch("http://localhost:1317/auth/accounts/" + this.state.user)
      .then(results => {
        return results.json();
      }).then(account => {
        let requestBody = JSON.stringify({
          base_req: {
            from: this.state.user,
            password: this.state.password,
            chain_id: 'tradechain',
            sequence: account.sequence,
            account_number: account.account_number
          },
          orderID: this.props.order.OrderID,
          owner: this.state.user
        });

        console.log(requestBody)

        fetch('http://localhost:1317/cosmostrade/orders/cancel', {
          method: 'POST',
          mode: 'no-cors',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: requestBody
        }).then(result => {
          console.log(result);
        });
      });


  }
}

export default Order