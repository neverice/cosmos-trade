import React, { Component } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default class LogIn extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {            
            show: false,
            owner: props.user,
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        if (this.state.owner != '') {
            this.setState( {owner: ''});
            this.props.handler('');
        }
        else {
            this.setState({ show: true });
        }
    }

    handleChange(event) {
        this.setState({ owner: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ show: false });
        this.props.handler(this.state.owner);
    }

    render() {
        const buttonName = this.state.owner == '' ? 'Log In' : 'Log Off';

        return (
            <>
                <Button variant="primary" onClick={this.handleShow} style={{float: 'right', marginTop: '20px'}}>{buttonName}</Button>

                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Log In</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <label>
                            Name: <input name="owner" type="text" value={this.state.owner} onChange={this.handleChange} />
                        </label>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
              </Button>
                        <Button variant="primary" onClick={this.handleSubmit}>
                            Log In
              </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}