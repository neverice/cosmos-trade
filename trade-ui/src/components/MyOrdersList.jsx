import React, { Component } from 'react'
import Order from './Order'

export default class MyOrdersList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            user: props.user,
        }
    }

    componentDidMount() {        
        fetch("http://localhost:1317/cosmostrade/orders/" + this.state.user)
            .then(results => {
                return results.json();
            }).then(data => {
                console.log(data)
                let orders = data.map((order) => {
                    return(
                        <div key={order.OrderID}>
                            <Order order={order} user={this.state.user}/>
                        </div>
                    )
                })
                this.setState({orders: orders})
            })
    }

    render() {
        return (            
            <div>
                {this.state.orders}
            </div>
        )
    }
}
