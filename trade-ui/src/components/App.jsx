import React, { Component } from 'react'

import OrderList from './OrderList'
import MyOrdersList from './MyOrdersList'
import NewOrder from './NewOrder'
import LogIn from './LogIn'
import 'bootstrap/dist/css/bootstrap.css'
import Cookies from 'universal-cookie';
import Button from 'react-bootstrap/Button';

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        const cookies = new Cookies();

        this.state = {
            user: cookies.get('User'),
            myOrdersMode: false
        };

        this.loginHandler = this.loginHandler.bind(this)

        this.handleOrdersClick = this.handleOrdersClick.bind(this);
        this.handleMyOrdersClick = this.handleMyOrdersClick.bind(this);
    }

    loginHandler(user){
        this.setState({user: user})
        const cookies = new Cookies();
        cookies.set('User', user);
    }    

    handleOrdersClick(){
        this.setState({myOrdersMode: false})
    } 
    
    handleMyOrdersClick(){
        this.setState({myOrdersMode: true})
    } 

    render() {
        let currentOrdersList;
        if (this.state.myOrdersMode){
            currentOrdersList = <MyOrdersList user={this.state.user} />
        } else {
            currentOrdersList = <OrderList user={this.state.user}/>
        }

        let newOrderButton;
        let myOrdersButton;
        if (this.state.user != ''){
            newOrderButton = <NewOrder user = {this.state.user}/>
            myOrdersButton = <Button variant="primary" onClick={this.handleMyOrdersClick} style={{marginRight: '5px'}}>My Orders</Button>
        }

        return (
            <div className="container">
                <div className="jumbotrone">
                    <h1 className="display-3">
                    Cosmos Trade 
                    <LogIn handler = {this.loginHandler} user = {this.state.user}/>
                    {newOrderButton}
                    </h1>
                </div>
                
                <hr/>
                <p>
                <Button variant="primary" onClick={this.handleOrdersClick} style={{marginRight: '5px'}}>Opened Orders</Button>
                {myOrdersButton}
                </p>
                {currentOrdersList}
            </div>
        )
    }
}

export default App