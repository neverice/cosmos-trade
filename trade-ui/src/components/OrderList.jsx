import React, { Component } from 'react'
import Order from './Order'

export default class OrderList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            user: props.user,
        }
    }

    componentDidMount() {
        fetch("http://localhost:1317/cosmostrade/orders")
            .then(results => {
                return results.json();
            }).then(data => {
                let orders = data.map((order) => {
                    if (this.state.user == '' || this.state.user != order.Owner)
                    return(
                        <div key={order.OrderID}>
                            <Order order={order} user={this.state.user}/>
                        </div>
                    )
                })
                this.setState({orders: orders})
            })
    }

    render() {
        return (            
            <div>
                {this.state.orders}
            </div>
        )
    }
}
