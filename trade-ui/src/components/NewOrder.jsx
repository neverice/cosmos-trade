import React from 'react'
import { Component } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default class NewOrder extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      show: false,
      owner: props.user,
      password: '',
      buy: '',
      sell: ''
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleChange(event) {
    if (event.target.name === 'password') {
      this.setState({ password: event.target.value });
    }
    if (event.target.name === 'buy') {
      this.setState({ buy: event.target.value });
    }
    if (event.target.name === 'sell') {
      this.setState({ sell: event.target.value });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    fetch("http://localhost:1317/auth/accounts/" + this.state.user)
      .then(results => {
        return results.json();
      }).then(account => {
        let requestBody = JSON.stringify({
          base_req: {
            from: this.state.owner,
            password: this.state.password,
            chain_id: 'tradechain',
            sequence: account.sequence,
            account_number: account.account_number
          },
          buy: this.state.buy,
          sell: this.state.sell,
          owner: this.state.owner
        });
        console.log(requestBody)

        fetch('http://localhost:1317/cosmostrade/orders/create', {
          method: 'post',
          mode: 'no-cors',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: requestBody
        }).then(function (response) {
          console.log(response);
        });
      });
    this.setState({ show: false });
  }

  render() {
    return (
      <>
        <Button variant="primary" onClick={this.handleShow} style={{ float: 'right', marginRight: '10px', marginTop: '20px' }}>Create Order</Button>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>New Order</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label>Buy:</label>
            <input name="buy" type="text" value={this.state.buy} onChange={this.handleChange} style={{ width: '100%', clear: 'both' }} /><br />

            <label>Sell:</label>
            <input name="sell" type="text" value={this.state.sell} onChange={this.handleChange} style={{ width: '100%', clear: 'both' }} /><br />

            <label>Password:</label>
            <input name="password" type="text" value={this.state.password} onChange={this.handleChange} style={{ width: '100%', clear: 'both' }} /><br />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancel
              </Button>
            <Button variant="primary" onClick={this.handleSubmit}>
              Create New Order
              </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}